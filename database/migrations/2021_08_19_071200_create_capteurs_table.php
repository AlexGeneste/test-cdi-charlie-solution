<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCapteursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('capteurs', function (Blueprint $table) {
            $table->id();
            $table->integer('message_id');
            $table->foreign('message_id')->references('id')->on('tbl_messages');
            $table->integer('gateway_id');
            $table->foreign('gateway_id')->references('id')->on('gateways');
            $table->timestamp('date');
            $table->string('capteur');
            $table->string('name');
            $table->string('rssi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('capteurs');
    }
}
