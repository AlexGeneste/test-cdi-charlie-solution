<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGatewaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gateways', function (Blueprint $table) {
            $table->integer('id')->primary();
            $table->integer('message_id');
            $table->foreign('message_id')->references('id')->on('tbl_messages');
            $table->string('latitude');
            $table->string('longitude');
            $table->timestamp('date');
            $table->string('gateway');
            //$table->integer('gateway_id');
            $table->string('battery_remaining');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gateways');
    }
}
