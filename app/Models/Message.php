<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $table = 'tbl_messages';
    protected $guarded = [];
    protected $primaryKey = 'id';
}
