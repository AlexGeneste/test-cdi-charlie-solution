<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Auth\Access\Gate;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Capteur extends Model
{
    public $timestamps = false;
    protected $guarded = [];

    public function message()
    {
        return $this->belongsTo(Message::class);
    }

    public function gateway()
    {
        return $this->belongsTo(Gateway::class);
    }

    public function scopeGroupByName($query)
    {
        return $query
            ->groupBy('name')
            ->paginate(10);
    }

    public function scopeCountAvgOutputWithName($query, $name)
    {
        /*return $query->select('*', DB::raw('COUNT(name) as avgOutput'))
            ->groupBy(DB::raw('DAY(date)'), 'name')
            ->get()
            ->avg('avgOutput');*/

        return DB::select("SELECT AVG(subQuery.nbOutput) as avgOutputByDay FROM (SELECT COUNT(name) as nbOutput FROM capteurs WHERE name = '" . $name . "' GROUP BY name, DAY(date)) as subQuery");
    }

    public function scopeThreeOrMoreOutput($query)
    {
        return $query
            ->select('*', DB::raw('COUNT(name) as nbOutput'))
            ->groupBy(DB::raw('DAY(date)'), 'name')
            ->get()
            ->where('nbOutput', '>', 3);
    }

    public function scopeNbOutputWithName($query, $name, $date)
    {
        return $query
            ->select(DB::raw('COUNT(name) as nbOutput'))
            ->where('name', $name)
            ->where(DB::raw('DAY(date)'), Carbon::parse($date)->format('d'))
            ->groupBy(DB::raw('DAY(date)'), 'name')
            ->get();
    }

    public function scopeWithGatewayId($query, $gatewayId)
    {
        return $query
            ->where('gateway_id', $gatewayId)
            ->get();
    }
}
