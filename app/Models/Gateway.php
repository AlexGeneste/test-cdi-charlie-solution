<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gateway extends Model
{
    public $timestamps = false;
    protected $guarded = [];

    public function message()
    {
        return $this->belongsTo(Message::class);
    }

    public function scopeWithLatitudeAndLongitude($query, $latitude, $longitude)
    {
        return $query
            ->where('latitude', $latitude)
            ->where('longitude', $longitude)
            ->paginate(10);
    }
}
