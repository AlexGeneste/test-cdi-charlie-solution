<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Capteur as CapteurModel;
use Livewire\WithPagination;

class Capteur extends Component
{
    private $capteur;

    public function mount(CapteurModel $capteur)
    {
        $this->capteur = $capteur;
    }

    public function render()
    {
        return view('livewire.capteur', [
            'capteurs' => $this->capteur->groupByName(),
        ]);
    }
}
