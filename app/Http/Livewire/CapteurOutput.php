<?php

namespace App\Http\Livewire;

use App\Models\Capteur as CapteurModel;
use Livewire\Component;

class CapteurOutput extends Component
{
    private $capteur;

    public function mount(CapteurModel $capteur)
    {
        $this->capteur = $capteur;
    }

    public function render()
    {
        return view('livewire.capteur-output', [
            'capteurs' =>$this->capteur->threeOrMoreOutput(),
        ]);
    }
}
