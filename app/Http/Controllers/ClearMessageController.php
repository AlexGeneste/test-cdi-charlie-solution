<?php

namespace App\Http\Controllers;

use App\Http\Service\ClearMessageService;
use App\Models\Message;
use Illuminate\Http\Request;

class ClearMessageController extends Controller
{
    private $clearMessageService;

    public function __construct(ClearMessageService $clearMessageService)
    {
        $this->clearMessageService = $clearMessageService;
    }


    public function import_message() : void
    {
        $this->clearMessageService->clearAllMessages();
    }
}
