<?php

namespace App\Http\Service;

use App\Models\Capteur;
use App\Models\Gateway;
use App\Models\Message;

class ClearMessageService
{
    private $message;
    private $gateway;
    private $capteur;

    // Injection des classes via Dependency Injection
    public function __construct(Message $message, Gateway $gateway, Capteur $capteur)
    {
        $this->message = $message;
        $this->gateway = $gateway;
        $this->capteur = $capteur;
    }

    public function clearAllMessages() : void
    {
        // Parcours de tous les messages
        foreach ($this->message->all() as $message) {
            $jsonMessage = json_decode($message->message);

            // Enregistrement des gateways
            if (substr($jsonMessage->s, 0,2) === 'GT') {
                $this->gateway->create([
                    'id' => trim($jsonMessage->v->ID_FRAME->value, 'idGT:'),
                    'message_id' => $message->id,
                    'latitude' => $jsonMessage->loc[0],
                    'longitude' => $jsonMessage->loc[1],
                    'date' => $jsonMessage->ts,
                    'gateway' => trim($jsonMessage->s, 'GT:'),
                    'battery_remaining' => $jsonMessage->v->battery->remaining,
                ]);
            }
            // Enregistrement des capteurs
            elseif (substr($jsonMessage->s, 0,2) === 'CP') {
                $this->capteur->create([
                    'message_id' => $message->id,
                    'gateway_id' => trim($jsonMessage->v->ID_FRAME->value, 'idGT:'),
                    'date' => $jsonMessage->ts,
                    'capteur' => trim($jsonMessage->s, 'CP:'),
                    'name' => $jsonMessage->v->NAME->value,
                    'rssi' => $jsonMessage->v->RSSI->value,
                ]);
            }
        }
    }
}
