<?php

use App\Http\Controllers\ClearMessageController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('content');
})->name('home');

Route::get('/clear-message', [ClearMessageController::class, 'import_message']);
Route::get('/map', function() {
    return view('map', [
        'gateways' => \App\Models\Gateway::where('latitude', '!=', 0)->where('longitude', '!=', 0)->get()->toJson(),
        'gatewaysWithLatAndLng' => \App\Models\Gateway::withLatitudeAndLongitude('49.2589732', '4.0375837'),
    ]);
})->name('map');
