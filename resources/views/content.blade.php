@extends('app')

@section('content')
    <div class="container w-full md:w-4/5 xl:w-3/5  mx-auto px-2">
        <div class="p-8 mt-6 lg:mt-0 rounded shadow bg-white">
            @livewire('capteur')
        </div>
    </div>

    <div class="container w-full md:w-4/5 xl:w-3/5  mx-auto px-2">
        <div class="p-8 mt-6 lg:mt-0 rounded shadow bg-white">
            @livewire('capteur-output')
        </div>
    </div>

    @livewireScripts
@endsection
