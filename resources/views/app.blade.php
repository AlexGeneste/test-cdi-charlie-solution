<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Test CDI</title>

        @livewireStyles
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <script src="{{ asset('js/app.js') }}"></script>

    </head>
    <body class="bg-gray-100 text-gray-900 tracking-wider leading-normal">
        @include('navbar')

       @yield('content')
    </body>
</html>
