@extends('app')

@section('content')
    <div class="flex justify-center h-screen mt-5">
        <div id="map" style="width: 1500px; height: 700px"></div>
    </div>

    <div class="container w-full md:w-4/5 xl:w-3/5  mx-auto px-2">
        <div class="p-8 mt-6 lg:mt-0 rounded shadow bg-white">
            <div class="flex flex-col">
                <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                    <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                        <div class="text-center">
                            <span class="font-sans text-xl">Liste de capteurs détécté dans la zon du chantier</span>
                        </div>
                        <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg mt-5">
                            <table class="min-w-full divide-y divide-gray-200" id="outputTable">
                                <thead class="bg-gray-50">
                                <tr>
                                    <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                        #
                                    </th>
                                    <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                        Nom du capteur
                                    </th>
                                    <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                        Date
                                    </th>
                                    <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                        Batterie restante du Gateway
                                    </th>
                                </tr>
                                </thead>
                                <tbody class="bg-white divide-y divide-gray-200">
                                @foreach ($gatewaysWithLatAndLng as $gateway)
                                    @foreach(\App\Models\Capteur::withGatewayId($gateway->id) as $capteur)
                                        <tr>
                                            <td class="px-6 py-4 whitespace-nowrap">
                                                <div class="flex items-center">
                                                    <div class="ml-4">
                                                        <div class="text-sm font-medium text-gray-900">
                                                            {{ $capteur->id }}
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="px-6 py-4 whitespace-nowrap">
                                                <div class="text-sm text-gray-900">{{ $capteur->name }}</div>
                                            </td>
                                            <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                                <div class="text-sm text-gray-900">{{ $gateway->date }}</div>
                                            </td>
                                            <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                                <div class="text-sm text-gray-900">{{ $gateway->battery_remaining }}</div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Fichiers Javascript -->
    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBpPu4Ddi9oqiT-o9id5QM6sp11FPyYpuE&callback=initMap&libraries=&v=weekly"
        async
    ></script>
    <script type="text/javascript">
        let map;
        let gatewayList = {!! $gateways !!};

        function initMap() {
            const myLatLng = { lat: 49.2589732, lng: 4.0375837 };
            const map = new google.maps.Map(document.getElementById("map"), {
                center: myLatLng,
                zoom: 15,
            });
            new google.maps.Marker({
                position: myLatLng,
                map,
                title: "Charlie Solution - Chantier",
            });
            new google.maps.Circle({
                radius: 1000,
                map,
                center: myLatLng,
            });
            console.log(gatewayList);
            gatewayList.forEach(gateway =>
                new google.maps.Marker({
                    position: { lat: parseFloat(gateway.latitude), lng: parseFloat(gateway.longitude) },
                    map,
                }),
            );
        }
    </script>
@endsection

