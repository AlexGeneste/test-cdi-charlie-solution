<nav class="flex items-center justify-between p-6 h-20 bg-white shadow-sm">
    <div class="py-5 px-3 rounded-full bg-gradient-to-r from-red-800 to-red-400 text-sm text-white font-semibold shadow-lg hover:cursor-pointer hover:shadow-lg">TEST CDI</div>
    <ul>
        <li class="space-x-5 text-xl">
            <a href="{{ route('home') }}" class="sm:inline-block text-gray-700 hover:text-indigo-700">Home</a>
            <a href="{{ route('map') }}" class="sm:inline-block text-gray-700 hover:text-indigo-700">Map</a>
        </li>
    </ul>
</nav>
