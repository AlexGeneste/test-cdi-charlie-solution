<h1>Test CDI</h1>

<div style="height: 100%; width: 100%;"><img src="https://charliesolutions.notion.site/image/https%3A%2F%2Fs3-us-west-2.amazonaws.com%2Fsecure.notion-static.com%2Fc6028c34-9f70-4f3d-bdc3-514b3868f2c3%2FUntitled.png?table=block&id=d21a3399-4441-405e-a57c-815b96c141f7&spaceId=f31dcc31-4df5-4303-9c38-18d501a3b597&width=4250&userId=&cache=v2" style="display: block; object-fit: cover; border-radius: 1px; width: 100%; pointer-events: auto;"></div>

<h2>Lancement du projet : </h2>
Il se déroule comme suit : 

Créer une base de données MySQL nommée test_cdi. Importer le script SQL test_technique.sql qui contient la table test_technique.sql qui contient la table tbl_messages avec la liste de message qui je suppose est envoyé par les gateways.

Dupliquer le fichier .env.example dans un fichier .env à la racine du projet (par exemple avec la commande suivante pour système Unix comme macOS pour moi : cp .env.example .env).

Modifier les informations suivantes afin de mettre vos propres informations de connexion pour la base de données :
```DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=8889
DB_DATABASE=test_cdi
DB_USERNAME=root
DB_PASSWORD=root
```

Importer les migrations à l'aide de la commande : 
```php artisan migrate```

Avant de pouvoir lancer l'application, il faut lancer cette commande : ````composer update```` afin d'installer les dépendances du projet.

<h2>Ressenti : </h2>

Dans le but de rendre les données plus lisibles, j'ai fait un traitement sur la table tbl_messages et j'ai séparé les données dans 2 tables qui correspondent aux Gateways et aux Capteurs.

J'ai choisi d'utiliser le pattern service afin de séparer la logique du controller. Dans ce pattern, le controller a pour but de faire la passerelle entre le fichier de route et la logique de l'application qui est contenue dans les services. J'aurai également pu incorper les Repository (qui aurait contenu uniquement les requêtes de base de données) mais il n'y avait pas de plus value dans cet environnement selon moi.

Laravel 8 incorpore un framework CSS nommé <a href="https://tailwindcss.com/">TailwindCSS</a> que je n'avais jamais utilisé et qui est plus difficile a prendre en main par rapport à Bootstrap comme j'ai pu le constater.
J'ai également utilisé <a href="https://laravel-livewire.com/">Laravel-Livewire</a> qui permet de faire des interfaces simples et dynamiques, c'était également une prise en main pour moi et je n'ai pas encore pu exploiter la puissance de cet outil.

Concernant mon ressenti, bien que j'ai compris les enjeux métiers, j'ai eu un peu de mal au niveau compréhension des données fournies dans la table tbl_messages. Notamment concernant le JSON stocké dans la table.
J'ai fait le choix d'utiliser des outils comme <a href="https://tailwindcss.com/">TailwindCSS</a> et <a href="https://laravel-livewire.com/">Laravel-Livewire</a>, ce sont des outils très puissant, je ne les avais jamais utilisé ce qui a donc engendré une partie prise en main de ces technologies et donc un temps considérable sur un projet d'une semaine. 
J'ai également pris énormément de temps sur la partie geo-fencing et un manque de compréhension sur la façon d'utilisation sur le terrain des différents dispositifs. J'ai cependant aimé travailler sur ce projet et ce nouveau secteur d'activité pour moi.
Comprenant que c'était un exercice de logique donné à plusieurs candidats et par souci d'equité, je n'ai donc pas pris la liberté de te poser les questions que j'aurai normalement poséés pour comprendre d'avantages. J'ai mis approximativement 20 heures.
